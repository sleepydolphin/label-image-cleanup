#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import nibabel as nib
import time

from label_image_cleanup.funcs import onehot


image = nib.load('../scripts/seg2.nii.gz').get_data()
labels = np.unique(image)

# loop
start = time.time()
label_image1 = [image == label for label in labels]
print('loop', time.time() - start)

# digitize
start = time.time()
label_image2 = np.digitize(image, labels, right=True)
print('digitize', time.time() - start)
label_image2 = onehot(label_image2, len(labels) - 1)
print('digitize', time.time() - start)

for l1, l2 in zip(label_image1, label_image2):
    assert np.array_equal(l1, l2)
