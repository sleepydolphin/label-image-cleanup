# Label Image Clean Up

Remove isolated pixels in a label image and fill holes. Build upon Shuo Han's [label-image-cleanup](https://gitlab.com/shan-utils/label-image-cleanup) with bugs fixed by Shimeng.

### Installation

```bash
$ pip install git+https://gitlab.com/sleepydolphin/label-image-cleanup.git
```



### Usage

See [dcumentation](https://shan-utils.gitlab.io/label-image-cleanup) for more details.

```bash
$ cleanup_label_image.py -i input.nii.gz -o output.nii.gz
```
